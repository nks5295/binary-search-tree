/**
 * This class is the shell for one node of a Binary Search Tree.
 * It holds the left and right nodes and the valuse itself. 
 * It has methods to set all three value and get all three values.
 * you can also construct a node without any left/right nodes
 * or with them.
 */

public class TreeNode {

	private int value;
	private TreeNode left;
	private TreeNode right;
	
	public TreeNode(int x){
		value = x;
		left = null;
		right = null;
	}
	
	public TreeNode(int x, TreeNode l, TreeNode r){
		value = x;
		left = l;
		right = r;
	}
	
	public void setValue(int x){
		value = x;
	}
	public void setLeft(TreeNode l){
		left = l;
	}
	public void setRight(TreeNode r){
		right = r;
	}
	
	public int getValue(){
		return value;
	}
	public TreeNode getLeft(){
		return left;
	}
	public TreeNode getRight(){
		return right;
	}
}
