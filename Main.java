/**
 * This class creates a Binary Search Tree, takes user input (numbers to add),
 * asks the user for options to mutate the tree, asks user what kind of traversal
 * they would like to do, and loops the program through.
 */

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Main {
	
	private static Tree tree; //the bianry search tree.

	public static void main(String[]args) throws IOException
	{
		tree = new Tree(); //creates new tree
		tree.setValuesList(); //creates an array within the tree to hold values
		greeting(); //prints a greeting for the user
		boolean repeat = true; //starts the program loop
		//this while loop loops the menu() method which loops the program
		while (repeat){
			repeat = menu(); //menu() is a boolean method that returns false when its time to quit
		}
	}
	
	/*
	 * This method asks the user how they want to mutate the tree.
	 * Options are insert values, pre/in/post order traversals,
	 * delete values, and quit. Uses an if..else statement to determine
	 * what the user enters and returns true whenever the choice isn't q
	 * and false when it is.
	 */
	public static boolean menu() throws IOException {
		System.out.println("i - insert value | 1 - pre order traversal | 2 - in order traversal");
		System.out.println("3 - post order traversal | d - delete value | q - quit");
		System.out.print(">> ");
		char choice = getInput().charAt(0);
		if (choice == 'i' || choice == 'I'){
			tree.insert(inputNumber());
		} else if (choice == '1'){
			System.out.println();
			tree.preOrderTraversal(tree.getRoot());
			System.out.println();
			return true;
		} else if (choice == '2'){
			System.out.println();
			tree.inOrderTraversal(tree.getRoot());
			System.out.println();
			return true;
		} else if (choice == '3'){
			System.out.println();
			tree.postOrderTraversal(tree.getRoot());
			System.out.println();
			return true;
		} else if (choice == 'd' || choice == 'D'){
			tree.deleteNode(inputNumber());
		} else if (choice == 'q' || choice == 'Q'){
			System.out.println();
			return false;
		} else {
			System.out.println("That wasn't a valid choice... Try again.");
			System.out.println();
			return true;
		}
		System.out.println();
		return true;
	}
	
	//prints a greeting for the user
	public static void greeting(){
		System.out.println("Binary Search Tree Program");
		System.out.println();
		System.out.println("This program builds and allows modifications to be made");
		System.out.println("to a binary search tree.");
		System.out.println();
	}
	
	//takes user input and converts it to int
	public static int inputNumber() throws IOException {
		System.out.print("Enter a number >> ");
		return Integer.parseInt(getInput());
	}
	
	//takes user input and returns strings
	public static String getInput() throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		return input.readLine();
	}
	
}
